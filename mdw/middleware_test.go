package mdw

import (
	"context"
	"fmt"
	"testing"
)

func TestMiddleWare(t *testing.T) {
	MiddlewareFunction := buildMiddleWare(Handle)
	resp, err := MiddlewareFunction(context.TODO(), "test")
	if err != nil {
		fmt.Println("main err:", err)
		return
	}
	fmt.Println("main resp:", resp)
}
