package creational

import (
	"fmt"
	"sync"
)

//https://zhuanlan.zhihu.com/p/48301256

type Singleton struct {
	Info string
}

var (
	once     sync.Once
	instance *Singleton
)

func GetInstance() *Singleton {
	once.Do(func() {
		instance = new(Singleton)
		instance.Info = "实例化"
		fmt.Println("instance...")
	})

	return instance
}

func UseSingleton() {
	//s := GetInstance()
	//
	//s["this"] = "that"
	//
	//s2 := GetInstance()
	//fmt.Println("This is ", s2["this"])

	s1 := GetInstance()
	fmt.Println(&s1)
	fmt.Println(s1.Info)

	s2 := GetInstance()
	fmt.Println(&s2)
	fmt.Println(s2.Info)
}
