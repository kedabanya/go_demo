package test

type AppIdDal struct {
	AppId int32
}

func (ad *AppIdDal) DBGetter() (interface{}, error) {
	return ad.AppId, nil
}

func GetAppId() (interface{}, error) {
	ad := &AppIdDal{}
	return ad.DBGetter()
}
