package test

import (
	"fmt"
	"github.com/agiledragon/gomonkey"
	"reflect"
	"testing"
)

//方法插桩
//go test -gcflags="all=-N -l" -v -run="TestAppIdDal_DBGetter"
func TestAppIdDal_DBGetter(t *testing.T) {
	ad := &AppIdDal{}
	patches := gomonkey.ApplyMethod(reflect.TypeOf(ad), "DBGetter", func(_ *AppIdDal) (interface{}, error) {
		return 15000, nil
	})
	defer patches.Reset()
	result, err := GetAppId()
	fmt.Println(result)
	fmt.Println(err)
}

//序列插桩
func TestAppIdDal_DBGetter_Seq(t *testing.T) {
	tests := []struct {
		name    string
		want    interface{}
		wantErr bool
	}{
		{"t1", 1128, false},
		{"t2", 1128, false},
		{"t3", 1234, false},
	}

	ad := &AppIdDal{}
	patches := gomonkey.ApplyMethodSeq(reflect.TypeOf(ad), "DBGetter", []gomonkey.OutputCell{
		{
			Values: []interface{}{1128, nil},
			Times:  2,
		},
		{
			Values: []interface{}{1234, nil},
			Times:  1,
		},
	})

	defer patches.Reset()

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetAppId()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAppID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAppID() got = %v, want %v", got, tt.want)
			}
		})
	}
}
