package test

import (
	"fmt"
	"github.com/agiledragon/gomonkey"
	"testing"
)

//函数插桩
//运行go test -gcflags="all=-N -l" -v -run="TestGetInfo"
//一定要gcflags参数，禁止内联，不然mock替换没办法生效
func TestGetInfo(t *testing.T) {
	patches := gomonkey.ApplyFunc(GetInfoStr, func() string {
		return "mock hello"
	})
	defer patches.Reset()
	result := GetInfo()
	fmt.Println(result)
}
