package emoji

import (
	"fmt"
	"regexp"
)

/**
参考：
   https://mrasong.com/a/emoji-regexp-for-go
   https://juejin.cn/post/6844903470617591822
   https://segmentfault.com/a/1190000022100299
*/
func CheckEmojiRegex(emoji string) {
	emojiReg := "[\\x{1F300}-\\x{1F64F}\\x{1F680}-\\x{1F6FF}\\x{2600}-\\x{2B55}\\s]"
	flag := regexp.MustCompile(emojiReg).MatchString(emoji)
	fmt.Println(flag)
}
