package slice

import (
	"fmt"
	"math/rand"
	"time"
)

//数组过滤加快
func SliceTrick() {
	//创建数据
	a := make([]int, 10000000, 10000000)
	for i := 0; i < 10000000; i++ {
		a = append(a, rand.Intn(5000))
	}

	//不用hack
	start1 := time.Now()
	b1 := make([]int, len(a), len(a))
	for _, x := range a {
		if x > 2500 {
			b1 = append(b1, x)
		}
	}
	end1 := time.Now()
	fmt.Printf("b1 time cost ==>%d ms\n", end1.Sub(start1).Milliseconds())

	fmt.Println()

	//使用hack
	start2 := time.Now()
	b2 := a[:0]
	for _, x := range a {
		if x > 2500 {
			b2 = append(b2, x)
		}
	}
	end2 := time.Now()
	fmt.Printf("b2 time cost ==>%d ms\n", end2.Sub(start2).Milliseconds())
}
