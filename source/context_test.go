package source

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func TestContext(t *testing.T) {
	root := context.Background()

	ctx1 := context.WithValue(root, "k1", 1111)
	ctx2, cancel1 := context.WithCancel(ctx1)
	defer cancel1()
	ctx3, cancel2 := context.WithTimeout(ctx2, 2*time.Second)
	defer cancel2()
	ctx4 := context.WithValue(ctx2, "k2", "22222")
	ctx5 := context.WithValue(ctx4, "k3", "33333")
	ctx6 := context.WithValue(ctx3, "k4", "4444")
	ctx7, cancel3 := context.WithDeadline(ctx4, time.Now().Add(4*time.Second))
	defer cancel3()
	ctx8, cancel4 := context.WithCancel(ctx6)
	defer cancel4()

	fmt.Println(ctx7.Value("k1"))
	fmt.Println(ctx6.Value("k4"))
	fmt.Println(ctx5.Value("k2"))

	fmt.Println(ctx7)

	go func() {
		select {
		case <-ctx7.Done():
			fmt.Println("ctx7 canceled")
		}
	}()

	go func() {
		select {
		// ctx8 canceled before ctx7, because ctx3 canceled
		case <-ctx8.Done():
			fmt.Println("ctx8 canceled")
		}
	}()

	time.Sleep(5 * time.Second)
}
